package se.hig.aod.lab2;

/**
 * Implementation of a LinkedList.
 * 
 * @author Robin Meleri
 * @version 2019-12-08
 */

public class LinkedList<T> implements ExtendedList<T> {

	private ListNode<T> head;

	private int numberOfElements;

	public LinkedList() {

		this.head = null;

	}

	@Override
	public boolean isEmpty() {
		if (head == null) {
			return true;
		}
		return false;
	}

	@Override
	public void clear() {

		head = null;
	}

	@Override
	public int numberOfElements() {
		if (head == null) {
			throw new ListIsEmptyException("Cant get number of elements from an empty list!");
		}
		ListNode<T> temp = head;
		numberOfElements = 0;

		while (temp != null) {
			numberOfElements++;
			temp = temp.nextNode;
		}

		return numberOfElements;
	}

	@Override
	public void insertFirst(T t) {
		if (head == null) {
			ListNode<T> ln = new ListNode<T>(t, null);
			head = ln;
		} else {
			if (head != null) {
				ListNode<T> ln1 = new ListNode<T>(t, head);
				head = ln1;
			}
		}
	}

	@Override
	public void insertLast(T t) {
		ListNode<T> temp = head;
		if (temp == null) {
			head = new ListNode<T>(t, null);
		} else {
			while (temp.nextNode != null) {
				temp = temp.nextNode;
			}
			temp.nextNode = new ListNode<T>(t, null);

		}

	}

	@Override
	public T removeFirst() {

		if (head == null) {
			throw new ListIsEmptyException("Cant remove first element from an empty list");
		}

		ListNode<T> temp = head;

		if (temp != null) {
			head = temp.nextNode;
		}

		return temp.t;
	}

	@Override
	public T removeLast() {
		if (head == null) {
			throw new ListIsEmptyException("Cant remove last element from an empty list!");
		}
		ListNode<T> temp = head;
		T t = null;

		while (temp.nextNode.nextNode != null) {
			temp = temp.nextNode;

		}
		t = temp.nextNode.t;
		temp.nextNode = null;
		return t;
	}

	@Override
	public T remove(int pos) {
		ListNode<T> temp = head;

		int index = 0;
		T t = null;

		while (index < pos) {
			index++;
			temp = temp.nextNode;
		}
		temp.t = temp.nextNode.t;

		return t;
	}

	@Override
	public T getFirst() {
		if (head == null) {
			throw new ListIsEmptyException("Cant get first element from an empty list!");
		}

		return head.t;
	}

	@Override
	public T getLast() {

		if (head == null) {
			throw new ListIsEmptyException("Cant get last element from an empty list!");
		}

		ListNode<T> temp = head;

		while (temp.nextNode != null) {
			temp = temp.nextNode;
		}
		return temp.t;

	}

	@Override
	public boolean contains(T t) {
		ListNode<T> temp = head;
		while (temp.nextNode != null && temp.t != t) {
			temp = temp.nextNode;
		}

		if (temp.t == t) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void printList() {

		ListNode<T> temp = head;
		while (temp != null) {
			System.out.println("Print using iteration: " + temp.t);
			temp = temp.nextNode;
		}
	}

	@Override
	public void printListR() {
		ListNode<T> temp = head;
		temp.printR();

	}

	@Override
	public void reversePrintList() {
		ListNode<T> temp = head;
		temp.revPrintR();

	}

	@Override
	public void insert(T t, int pos) {
		if (isEmpty() && pos == 0) {
			insertFirst(t);
		}

		int index = 0;
		ListNode<T> temp = head;

		while (index < pos - 1) {
			index++;
			temp = temp.nextNode;
		}

		ListNode<T> ln = new ListNode<T>(t, temp.nextNode);
		temp.nextNode = ln;

	}

	@Override
	public T get(int pos) {

		if (isEmpty()) {
			throw new ListIsEmptyException("Can't get element from empty list");
		}
		ListNode<T> temp = head;

		int index = 0;

		while (index != pos) {
			index++;
			temp = temp.nextNode;

		}

		return temp.t;
	}

}
