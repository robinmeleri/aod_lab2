package se.hig.aod.lab2;

/**
 * Exception class that signals if an operation that is not allowed has been
 * performed on a queue that is full.
 * 
 * @author Robin Meleri
 * @version 2019-12-08
 */

@SuppressWarnings("serial")
public class FullQueueException extends RuntimeException {

	/**
	 * The constructor takes a message about a specific error that has been
	 * generated. The message can be written to the user when the exception is
	 * catched
	 */

	public FullQueueException(String message) {

	}

}
