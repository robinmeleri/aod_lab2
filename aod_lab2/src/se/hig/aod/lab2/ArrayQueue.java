package se.hig.aod.lab2;

/**
 * Array implementation of a Queue. 
 * 
 * @author Robin Meleri
 * @version 2019-12-05
 */

public class ArrayQueue<T> implements Queue<T> {
	
	

	private T[] arr;
	private int back;
	private int front;
	private int size;
	private int currentSize;

	@SuppressWarnings("unchecked")
	public ArrayQueue(int size) {
		
		this.size = size;
		front = 0;
		back = 0;
		currentSize = 0;
		arr = (T[]) new Object[this.size];
	}

	@Override
	public void clear() {

		for (int i = 0; i < arr.length; i++) {
			arr[i] = null;
		}
		back = 0;
		front = 0;
		currentSize = 0;

	}

	@Override
	public boolean isEmpty() {

		if (currentSize == 0) {
			return true;
		}

		return false;
	}

	public boolean isFull() {
		if (currentSize == size) {
			return true;
			
		} else {
			return false;
		}

	}

	@Override
	public void enqueue(T t) {

		if (currentSize == size) {
			throw new FullQueueException("Can't add element to full stack");
		}
		arr[back] = t;
		back = (back + 1) % (arr.length);
		currentSize++;
		

	}

	@Override
	public T dequeue() {

		T temp;
		if (front == back) {
			throw new EmptyQueueException("Can't remove element from empty stack");
		}
		temp = arr[front];
		front = (front + 1) % (arr.length);
		currentSize--;
		return temp;

	}

	@Override
	public T getFront() {
		if(arr[front] == null) {
			throw new EmptyQueueException("Cant get element from empty stack");
		}

		return arr[front];
	}

}
