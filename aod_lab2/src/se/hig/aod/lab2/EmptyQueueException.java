package se.hig.aod.lab2;

/**
 * Exception class that signals if an operation that is not allowed has been
 * performed on an queue that is empty.
 * 
 * @author Robin Meleri
 * @version 2019-12-08
 */

@SuppressWarnings("serial")
public class EmptyQueueException extends RuntimeException {

	/**
	 * The constructor takes a message about a specific error that has been
	 * generated. The message can be written to the user when the exception is
	 * catched
	 */

	public EmptyQueueException(String message) {
		super(message);
	}
	
}
