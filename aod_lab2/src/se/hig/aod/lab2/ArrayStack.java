/**
 * 
 */
package se.hig.aod.lab2;

/**
 * Array implementation of a Stack. 
 * 
 * @author Robin Meleri
 * @version 2019-12-05
 *
 */
public class ArrayStack<T> implements Stack<T> {

	private T[] stack;
	private int size;
	private int top;

	@SuppressWarnings("unchecked")
	public ArrayStack(int size) {
		this.size = size;
		stack = (T[]) new Object[this.size];
		this.top = -1;
	}

	@Override
	public void clear() {

		for (int i = 0; i < stack.length; i++) {
			stack[i] = null;
		}

		top = -1;

	}

	@Override
	public boolean isEmpty() {
		if (top == -1) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean isFull() {

		if (top == size - 1) {
			return true;
		} 
		else {
			return false;
		}
	}

	@Override
	public void push(T t) {

		if (top == size - 1) {
			throw new FullStackException("Cant push element to full stack");
		} else {

			top = top + 1;
			stack[top] = t;
		}

	}

	@Override
	public T pop() {
		if (isEmpty()) {
				throw new EmptyStackException("Cant pop element from empty stack");
		} else {

			top = top - 1;

		}
		return stack[top];

	}

	@Override
	public T top() {

		if (top == -1) {
			throw new EmptyStackException("Cant peek into an empty stack");
		}

		return stack[top];
	}

}
