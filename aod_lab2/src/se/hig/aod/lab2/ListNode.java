package se.hig.aod.lab2;
/**
 * ListNode-class that represents a node in a list.
 * 
 * @author Robin Meleri
 * @version 2019-12-05
 *
 */
class ListNode<T> {
	T t;
	ListNode<T> nextNode;

	ListNode(T t, ListNode<T> nextNode) {
		this.t = t;
		this.nextNode = nextNode;
	}

	ListNode(T t) {
		this.t = t;
	}

	public void printR() {
		System.out.println("Print using recursive: " + t);
		if (nextNode != null) {
			nextNode.printR();
		}

	}

	public void revPrintR() {
		if (nextNode != null) {

			nextNode.revPrintR();
		}
		System.out.println("Print reverse: " + t);
	}
}