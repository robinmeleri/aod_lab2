package se.hig.aod.lab2;

/**
 * Exception class that signals if an operation that is not allowed has been performed on
 * an stack that is full.
 * 
 * @author Robin Meleri
 * @version 2019-12-06
 */

@SuppressWarnings("serial")
public class EmptyStackException extends RuntimeException {
	
	/**
	 * The constructor takes a message about a specific error that has been generated.
	 * The message can be written to the user when the exception is catched
	 */
	
	public EmptyStackException(String message) {
		super(message);
	}

}
