/*
 * This is free to use in courses at University of Gävle.
 */
package se.hig.aod.lab2;

/**
 * An interface for the generic Abstract Data Type <code>Queue</code>.
 * 
 * @author Robin Meleri
 * @author Magnus Hjelmblom
 * @author Anders Jackson
 * @author Peter Jenke
 * @version 2014-01-16
 * @version 2019-12-08
 */

public interface Queue<T>
{
    /** 
     * Removes all elements from this queue and writes 
     * a message on {@link System}.out).
     */
    public void clear ();
    
	/**
	 * Checks if this queue is empty.
	 * 
	 * @return true if queue is empty, false otherwise.
	 */
	public boolean isEmpty ();

	/**
	 * Check if this queue is full.
	 * 
	 * @return true if queue is full, false otherwise.
	 */
	
	public boolean isFull();
	
	/**
	 * Add an element at the last position in this queue.
	 * 
	 * @param object added to this queue.
	 */
	
	public void enqueue (T t);

	/**
	 * Removes the first element from this queue. Will throw
	 * {@link QueueEmptyException} if this queue is empty.
	 * 
	 * @return the first element.
	 */
	public T dequeue ();

	/**
	 * Returns the first element in this queue, without removing it. Will throw
	 * {@link QueueEmptyException} if this queue is empty.
	 * 
	 * @return the first element.
	 */
	public T getFront ();
}
